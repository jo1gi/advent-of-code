import Data.List.Unique
import Data.List

main = do
    contents <- readFile "input.txt"
    let
        a = calc (0, 0, [(0,0)]) contents
        b = calc (0, 0, [(0,0)]) $ tail contents
    print $ length $ sortUniq (a++b)

calc :: (Int, Int, [(Int, Int)]) -> [Char] -> [(Int, Int)]
calc (_,_,x) [] = x
calc (x, y, values) (c:_:cs) =
    let
        (changeX, changeY) = case c of
            '^' -> (0,1)
            'v' -> (0,-1)
            '>' -> (1,0)
            '<' -> (-1,0)
            _ -> (0,0)
        newX = x+changeX
        newY = y+changeY
        newValues = (newX, newY):values
    in
        if (length cs < 2) then newValues else calc (newX, newY, newValues) cs
