import Data.List.Unique
import Data.List

main = do
    contents <- readFile "input.txt"
    let (_,_,houses) = calc (0, 0, [(0,0)]) contents
    print $ length $ sortUniq houses


calc :: (Int, Int, [(Int, Int)]) -> [Char] -> (Int, Int, [(Int, Int)])
calc x [] = x
calc (x, y, values) (c:cs) =
    let
        (changeX, changeY) = case c of
            '^' -> (0,1)
            'v' -> (0,-1)
            '>' -> (1,0)
            '<' -> (-1,0)
            _ -> (0,0)
        newX = x+changeX
        newY = y+changeY
    in
        calc (newX, newY, (newX, newY):values) cs

