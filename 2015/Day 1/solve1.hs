main = do
    contents <- readFile "input.txt"
    let result = foldr calc 0 contents
    putStrLn $ show result

calc :: Char -> Int -> Int
calc '(' x = x+1
calc ')' x = x-1
calc _ x = x
