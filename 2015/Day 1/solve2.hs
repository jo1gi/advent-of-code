main = do
    contents <- readFile "input.txt"
    let result = calc contents 0 0
    putStrLn $ show result

calc :: [Char] -> Int -> Int -> Int
calc [] _ _ = 0
calc _ (-1) pos = pos
calc ('(':xs) floor pos = calc xs (floor+1) (pos+1)
calc (')':xs) floor pos = calc xs (floor-1) (pos+1)
calc (_:xs) floor pos = calc xs (floor) (pos+1)
