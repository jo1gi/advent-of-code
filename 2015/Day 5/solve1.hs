main = do
    contents <- readFile "input.txt"
    let result = foldr countNice 0 $ lines contents
    putStrLn $ show result

countNice :: String -> Int -> Int
countNice input count
    | hasThreeVowels x && containsPair x && (not $ containsBadPair x) = count+1
    | otherwise = count

hasThreeVowels :: String -> Bool
hasThreeVowels x = 3 <= (length $ filter (\c -> elem c "aeiou") x)

containsPair :: String -> Bool
containsPair input
    | (length input) < 2 = False
    | otherwise =
        let (x:y:xs) = input
        in x== y || containsPair (y:xs)

containsBadPair :: String -> Bool
containsBadPair input
    | (lenght input) < 2 = False
    | otherwise =
        let (x:y:xs) = input
        in elem (x:y:[]) ["ab", "cd", "pq", "xy"] || containsBadPair (y:xs)
