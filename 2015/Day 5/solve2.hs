main = do
    contents <- readFile "input.txt"
    let result = foldr countNice 0 $ lines contents
    putStrLn $ show result

countNice :: String -> Int -> Int
countNice x count
    | hasTriple x && hasDoublePair "" x = count + 1
    | otherwise = count


hasDoublePair :: String -> String -> Bool
hasDoublePair found input
    | 1 >= (length input) = False
    | found == "" =
        let
            (x:y:xs) = input
            new = [x,y]
        in
            hasDoublePair new xs || hasDoublePair "" (y:xs)
    | otherwise =
        let (x:y:xs) = input
        in found == [x,y] || hasDoublePair found (y:xs)

hasTriple :: String -> Bool
hasTriple input
    | 3 > (length input) = False
    | otherwise =
        let (x:y:z:xs) = input
        in x == z || hasTriple (y:z:xs)
