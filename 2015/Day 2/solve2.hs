import Data.List
import Data.List.Split

main = do
    contents <- readFile "input.txt"
    print $ sum $ map calcRibbon $ lines contents

parseDimensions :: String -> [Int]
parseDimensions dimensions = map (read) $ splitOn "x" dimensions

calcRibbon :: String -> Int
calcRibbon dimensions =
    let
        dims = parseDimensions dimensions
        (l,w,h) = (dims!!0,dims!!1,dims!!2)
        a = minimum [l,w,h]
        b = minimum (delete a [l,w,h])
    in
        (l*w*h) + (2*a) + (2*b)
