import Data.List.Split

main = do
    contents <- readFile "input.txt"
    print $ sum $ map calcPaper $ lines contents

parseDimensions :: String -> [Int]
parseDimensions dimensions = map (read) $ splitOn "x" dimensions

calcPaper :: String -> Int
calcPaper dimensions =
    let
        dims = parseDimensions dimensions
        (l,w,h) = (dims!!0,dims!!1,dims!!2)
        (a,b,c) = (l*w, w*h, h*l)
        smallest = minimum [a,b,c]
    in
        (2*a)+(2*b)+(2*c)+smallest
