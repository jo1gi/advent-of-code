let file = "input"

let input_to_grid input =
  String.split_on_char '\n' input
  |> List.map (fun x -> String.to_seq x |> Seq.map (fun c -> Char.code c - 48) |> List.of_seq)

let range i j =
  let rec aux n acc =
    if n < i then acc else aux (n-1) (n :: acc)
  in aux j [] ;;

let grid_index grid x y =
  (List.nth (List.nth grid x) y)

let trees_in_direction grid x y r f =
  let range_size = List.length r in
  let visible_trees = r |> List.to_seq |> Seq.take_while (fun i -> (f x y i) < (grid x y)) |> Seq.length in
  let result = if visible_trees == range_size then visible_trees else succ visible_trees in
  result

let scenic_score grid x y =
  let g = grid_index grid in
  let size = (List.length grid)-2 in
  (trees_in_direction g x y (range 0 (x-1) |> List.rev) (fun x y i -> (g i y))) *
  (trees_in_direction g x y (range (x+1) size) (fun x y i -> (g i y))) *
  (trees_in_direction g x y (range 0 (y-1) |> List.rev) (fun x y i -> (g x i))) *
  (trees_in_direction g x y (range (y+1) size) (fun x y i -> (g x i)))

let max l =
  List.fold_left Int.max 0 l

let max_scenic_score grid =
  let size = (List.length grid) - 2 in
  List.map
    (fun x -> List.map
      (fun y -> scenic_score grid x y)
      (range 0 size)
      |> max)
    (range 0 size)
  |> max

let () =
  let ic = open_in file in
  try
    let grid = In_channel.input_all ic |> input_to_grid in
    (* scenic_score grid 2 1 |> Int.to_string |> print_endline; *)
    (* print_scenic_score grid |> print_endline; *)
    (* print_forest grid |> print_endline; *)
    max_scenic_score grid |> Int.to_string |> print_endline;
      close_in ic
  with e ->
    close_in_noerr ic;
      raise e
