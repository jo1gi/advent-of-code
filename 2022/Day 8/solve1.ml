Printexc.record_backtrace false
let file = "input"

let input_to_grid input =
  String.split_on_char '\n' input
  |> List.map (fun x -> String.to_seq x |> Seq.map (fun c -> Char.code c - 48) |> List.of_seq)

(* let rec range a b = *)
(*   if a >= b then [] *)
(*   else a :: range (a + 1) b *)
let range i j =
  let rec aux n acc =
    if n < i then acc else aux (n-1) (n :: acc)
  in aux j [] ;;

let grid_index grid x y =
  (List.nth (List.nth grid x) y)

let can_be_seen grid x y =
  let g = grid_index grid in
  let size = (List.length grid)-2 in
  let height = g x y in
  range 0 (x-1) |> List.for_all (fun i -> (g i y) < height)
  || range (x+1) size |> List.for_all (fun i -> (g i y) < height)
  || range 0 (y-1) |> List.for_all (fun i -> (g x i) < height)
  || range (y+1) size |> List.for_all (fun i -> (g x i) < height)

let count_visible_trees grid =
  let size = (List.length grid) - 2 in
  List.map
    (fun x -> List.filter
      (fun y -> can_be_seen grid x y)
      (range 0 size)
      |> List.length)
    (range 0 size)
  |> List.fold_left (fun a b -> a + b) 0

let print_visible_trees grid =
  let size = (List.length grid) - 2 in
  List.map
    (fun x -> List.map
      (fun y -> if can_be_seen grid x y then "t" else "f")
      (range 0 size)
      |> String.concat "")
    (range 0 size)
  |> String.concat "\n"

let () =
  let ic = open_in file in
  try
    let grid = In_channel.input_all ic |> input_to_grid in
    count_visible_trees grid |> Int.to_string |> print_endline;
      close_in ic
  with e ->
    close_in_noerr ic;
      raise e
