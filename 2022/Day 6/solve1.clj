(require '[clojure.string :as string])

(defn contains-duplicates? [x]
  (not= (count (set x)) (count x)))

(defn find-start-of-packet-marker [input marker-size]
  (->> input
       (partition marker-size 1)
       (remove contains-duplicates?)
       (first)
       (string/join)
       (string/index-of input)
       (+ marker-size)))

(-> (slurp "input")
    (find-start-of-packet-marker 4))

(-> (slurp "input")
    (find-start-of-packet-marker 14))
