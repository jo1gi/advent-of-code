(require '[clojure.string :as string])

(defn parse-line [line]
  (let [x (string/split line #" ")]
    [ ({"A" :R "B" :P "C" :S} (x 0))
     ({"X" :R "Y" :P "Z" :S} (x 1))]))

(defn parse-input [input]
  (->> (string/split input #"\n")
       (map parse-line)))

(defn winner-points [[opp you]]
  (if (= opp you)
    3
    (case [opp you]
      [:R :P] 6
      [:P :S] 6
      [:S :R] 6
      [x x] 3
      0)))

(defn calc-score [[opp you]]
  (+
   ({:R 1 :P 2 :S 3} you)
   (winner-points [opp you])))

(defn calc-total-score [rounds]
  (->> rounds
       (map calc-score)
       (reduce +)))

(-> (slurp "input")
    (parse-input)
    (calc-total-score))
