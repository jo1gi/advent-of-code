(require '[clojure.string :as string])

(defn parse-line [line]
  (let [x (string/split line #" ")]
    [ ({"A" :R "B" :P "C" :S} (x 0))
     (x 1)]))

(defn parse-input [input]
  (->> (string/split input #"\n")
       (map parse-line)))

(defn winner-points [you]
  (case you
    "X" 0
    "Y" 3
    "Z" 6))

(defn index-of [item coll]
  (count (take-while (partial not= item) coll)))

(defn find-choice [opp you]
  (let [l [:R :P :S]]
    (l
     (mod
       (+ (index-of opp l)
        ({"X" -1 "Y" 0 "Z" 1} you))
       3))))


(defn calc-score [[opp you]]
  (+
   ({:R 1 :P 2 :S 3} (find-choice opp you))
   (winner-points you)))

(defn calc-total-score [rounds]
  (->> rounds
       (map calc-score)
       (reduce +)))

(-> (slurp "input")
    (parse-input)
    (calc-total-score))
