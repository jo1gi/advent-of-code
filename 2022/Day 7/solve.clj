(require '[clojure.string :as string])

(defn lines-to-command-output [input]
  (let [lines (map #(string/split % #" ")(string/split input #"\n"))]
    {:cmd (first lines)
     :output (rest lines)}))

(defn parse-input [input]
  (->> (string/split input #"\$ ")
       (rest)
       (map lines-to-command-output)))

(defn create-file [[x name]]
  {name
   (if (= x "dir")
     {}
     (Integer/parseInt x))})

(defn add-command-to-tree [[tree path] command]
  (let [cmd (command :cmd)
        output (command :output)]
    (case (first cmd)
      "cd" [tree
            (if (= (last cmd) "..")
              (vec (drop-last path))
              (conj path (last cmd)))]
      "ls" [(update-in tree path (constantly (into (sorted-map) (map create-file output))))
            path])))

(defn create-tree [commands]
  (get-in (reduce add-command-to-tree [{} []] commands) [0 "/"]))

(defn calc-dir-size [tree]
  (->> tree
       (map
         (fn [[title content]]
           (if (int? content)
             content
             (calc-dir-size (tree title)))))
       (reduce +)))

(defn get-subdirs [tree path]
  (->> tree
       (map (fn [[title content]]
              (if (int? content)
                []
                (conj
                  (get-subdirs
                    (tree title)
                    (conj path title))
                  (conj path title)))))
       (apply concat)))

(defn get-all-dirs [tree]
  (get-subdirs tree []))

(defn calc-subdir-size [tree path]
  (calc-dir-size (get-in tree path)))


; Part 1
(let [input (slurp "input")
      commands (parse-input input)
      tree (create-tree commands)]
  (->>
    (get-all-dirs tree)
    (map #(calc-subdir-size tree %))
    (filter #(<= % 100000))
    (reduce +)))

(def total-size 70000000)
(def minimum-size 30000000)

; Part 2
(let [input (slurp "input")
      commands (parse-input input)
      tree (create-tree commands)
      storage-used (calc-dir-size tree)]
  (->>
    (get-all-dirs tree)
    (map #(calc-subdir-size tree %))
    (filter #(>= % (- minimum-size (- total-size storage-used))))
    (sort)
    (first)))
