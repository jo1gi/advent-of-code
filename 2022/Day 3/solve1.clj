(require '[clojure.string :as string]
         '[clojure.set :as set])

(defn get-compartments [line]
  (let [length (count line)
        mid (/ length 2)]
    [(subs line 0 mid)
     (subs line mid length)]))

(defn common-items [[left right]]
  (set/intersection (set left) (set right)))

(defn score [c]
  (- (int c)
     (if (Character/isUpperCase c)
       38
       96)))

(->> (slurp "input")
     (#(string/split % #"\n"))
     (map get-compartments)
     (map common-items)
     (apply concat)
     (map score)
     (reduce +))
