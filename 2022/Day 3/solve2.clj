(require '[clojure.string :as string]
         '[clojure.set :as set])

(defn common-item [[a b c]]
  (set/intersection (set a) (set b) (set c)))

(defn score [c]
  (- (int c)
     (if (Character/isUpperCase c)
       38
       96)))


(->> (slurp "input")
     (#(string/split % #"\n"))
     (partition 3)
     (map vec)
     (map common-item)
     (apply concat)
     (map score)
     (reduce +))
