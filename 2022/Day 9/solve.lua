function load_input(file)
    local lines = {}
    for line in io.lines(file) do
        local direction = string.sub(line, 0, 1)
        local amount = tonumber(string.sub(line, 3))
        lines[#lines + 1] = {direction, amount}
    end
    return lines
end

function move_head(head, direction)
    if(direction=="R") then
        head[1]=head[1]+1
    elseif(direction=="U") then
        head[2]=head[2]+1
    elseif(direction=="L") then
        head[1]=head[1]-1
    elseif(direction=="D") then
        head[2]=head[2]-1
    end
end

function move_tail(tail, head)
    local xdist = math.abs(tail[1]-head[1])
    local ydist = math.abs(tail[2]-head[2])
    if(xdist==0 and ydist==0) then
        return
    elseif(xdist==0) then
        if(tail[2]>head[2]) then
            tail[2]=head[2]+1
        else
            tail[2]=head[2]-1
        end
    elseif(ydist==0) then
        if(tail[1]>head[1]) then
            tail[1]=head[1]+1
        else
            tail[1]=head[1]-1
        end
    elseif(xdist==2 and ydist==2) then
        tail[1]=head[1]+(tail[1]-head[1])/2
        tail[2]=head[2]+(tail[2]-head[2])/2
    elseif(xdist==2) then
        tail[2]=head[2]
        tail[1]=head[1]+(tail[1]-head[1])/2
    elseif(ydist==2) then
        tail[1]=head[1]
        tail[2]=head[2]+(tail[2]-head[2])/2
    end
end

function tail_position_exists(tails, tail)
    for _,t in pairs(tails) do
        if(t[1]==tail[1] and t[2]==tail[2]) then
            return true
        end
    end
    return false
end

function move_rope(moves, tail_size)
    local rope = {}
    local tails = {}
    for i=1,tail_size do
        rope[i] = {0,0}
    end
    for _,move in pairs(moves) do
        local direction = move[1]
        local amount = move[2]
        for i=1,amount do
            move_head(rope[1], direction)
            for i=2,tail_size do
                move_tail(rope[i], rope[i-1])
            end
            local tail = rope[tail_size]
            if(not tail_position_exists(tails, tail))then
                tails[#tails + 1] = {tail[1], tail[2]}
            end
        end
    end
    return #tails
end

function solve(file, tail_size)
    local input = load_input(file)
    return move_rope(input, tail_size)
end

-- Part 1
solve("test", 2)
solve("input", 2)
-- Part 2
solve("test2", 10)
solve("input", 10)
