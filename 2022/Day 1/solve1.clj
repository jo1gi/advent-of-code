(require '[clojure.string :as string])

(defn parse-elf [input]
  (->> (string/split input #"\n")
       (map read-string)))

(defn get-elfs [input]
  (->> (string/split input #"\n\n")
       (map parse-elf)))


(->> (slurp "input")
     (get-elfs)
     (map #(reduce + %))
     (sort)
     (last))
