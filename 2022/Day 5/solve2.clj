(require '[clojure.string :as string])

(defn parse-operation [line]
  (let [parts (string/split line #" ")
        f #(Integer/parseInt (parts %))]
    {:amount (f 1) :from (dec (f 3)) :to (dec (f 5))}))

(defn parse-operations [lines]
  (->> (string/split lines #"\n")
       (map parse-operation)))

(defn count-crate-stacks [line]
  (/ (inc (count line)) 4))

(defn parse-initial-line [line]
  (map #(get line (inc (* 4 %))) (range (count-crate-stacks line))))

(defn add-char [setup line index]
    (if (= (line index) \space)
      (setup index)
      (conj (setup index) (line index))))

(defn add-state-line [setup line]
  (->> (range (count line))
       (map #(add-char setup line %))
       (vec)))

(defn parse-initial-state [lines]
  (->> (string/split lines #"\n")
       (drop-last)
       (map parse-initial-line)
       (map vec)
       (reduce add-state-line (vec (repeat 9 [])))
       ))

(defn move-crates [state {amount :amount to :to from :from}]
  (-> state
      (update to #(concat (take amount (state from)) %) )
      (update from #(drop amount %))
      ))

(defn msg [state]
  (string/join (map first state)))

(let [init (parse-initial-state "    [D]    
[N] [C]    
[Z] [M] [P]
 1   2   3 ")
      operations [(parse-operation "move 1 from 2 to 1")
                  (parse-operation "move 3 from 1 to 3")
                  (parse-operation "move 2 from 2 to 1")
                  (parse-operation "move 1 from 1 to 2")
                  ]]
  (msg (reduce move-crates init operations))
  )

(let [input (slurp "input")
      parts (string/split input #"\n\n")
      state (parse-initial-state (parts 0))
      operations (parse-operations (parts 1))]
  ; (move-crates state (nth operations 0))
  (msg (reduce move-crates state operations))
  )
