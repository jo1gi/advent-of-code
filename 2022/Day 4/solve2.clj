(require '[clojure.string :as string]
         '[clojure.set :as set])

(defn numbers-in-section [start end]
  (set (range start (inc end))))

(defn parse-section [section]
  (let [[a b] (string/split section #"-")]
    (numbers-in-section (Integer/parseInt a) (Integer/parseInt b))))

(defn parse-line [line]
  (let [[x y] (string/split line #",")]
    [(parse-section x) (parse-section y)]))

(defn parse-lines [lines]
  (->> (string/split lines #"\n")
       (map parse-line)))

(defn pair-containing-other? [[a b]]
  (not= 0 (count (set/intersection a b))))

(->> (slurp "input")
     (parse-lines)
     (filter pair-containing-other?)
     (count)
     )
