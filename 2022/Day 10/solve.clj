(require '[clojure.string :as string])

(defn parse-line [line]
  (let [parts (string/split line #" ")]
    (if (= (count parts) 2)
      {:op (parts 0) :value (Integer/parseInt (parts 1))}
      {:op (parts 0)})))

(defn parse-input [input]
  (->> (string/split input #"\n")
       (map parse-line)))

(defn cycles-from-operation [op]
  (if (= (op :op) "noop")
    [0]
    [0 (op :value)]))

(defn signal-strength-positions [cycles]
  (-> (count cycles)
      (- 20)
      (/ 40)
      (Math/floor)
      (int)
      (inc)
      ((fn [x] (map #(+ 20 (* % 40)) (range 0 x))))))

(defn calc-signal-strength-pos [cycles pos]
  (->> (take (dec pos) cycles)
       (reduce + 1)
       (* pos)))

(defn signal-strength [cycles]
  (let [pos (signal-strength-positions cycles)]
    (map #(calc-signal-strength-pos cycles %) pos)))

(defn map-cycles-to-pixels [cycles]
  (map
    (fn [i]
      (if (<= (Math/abs (- (mod i 40) (cycles i))) 1)
        "#"
        "."))
    (range 0 (count cycles))))

(defn pixels-to-screen [pixels]
  (->> (partition 40 pixels)
       (map #(String/join "" %))
       (String/join "\n")))

; Part 1
(->> (slurp "input")
     (parse-input)
     (map cycles-from-operation)
     (apply concat)
     (signal-strength)
     (reduce +)
     )

; Part 2
(->> (slurp "input")
     (parse-input)
     (map cycles-from-operation)
     (apply concat)
     (reductions + 1)
     (vec)
     (map-cycles-to-pixels)
     (pixels-to-screen)
     (println))
