use std::io::prelude::*;

fn calc_row_or_col(s: &str, upper: char, mut max: f64) -> i32 {
    let mut min: f64 = 0.0;
    for i in s.chars() {
        let avg = (max+min)/2.0;
        if i == upper {
            min = avg.ceil();
        } else {
            max = avg.floor();
        }
    }
    return min as i32;
}

fn calc_row_and_col(ticket: &str) -> (i32, i32) {
    let row_str = &ticket[0..7];
    let col_str = &ticket[7..10];
    let row = calc_row_or_col(&row_str, 'B', 127.0);
    let col = calc_row_or_col(&col_str, 'R', 7.0);
    return (row, col);
}

fn calc_ticket_id(ticket: &str) -> i32 {
    let (row, col) = calc_row_and_col(ticket);
    return row * 8 + col;
}

fn main() -> std::io::Result<()> {
    let mut file = std::fs::File::open("input")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let mut tickets: Vec<i32> = contents.lines()
        .map(calc_ticket_id)
        .collect();
    tickets.sort();
    for i in 0..tickets.len() {
        let ticket = tickets[i]+1;
        if ticket != tickets[i+1] {
            println!("{}", ticket);
            break;
        }
    }
    Ok(())
}
