use std::io::prelude::*;

fn split_string(s: &str) -> Vec<i32> {
    let mut nums = Vec::new();
    for i in s.split("\n") {
        nums.push(match i.parse::<i32>() {
            Ok(n) => n,
            Err(_) => continue,
        })
    }
    return nums;
}

fn find_nums(nums: &[i32]) -> (i32, i32) {
    for i in 0..nums.len() {
        for j in i+1..nums.len() {
            if nums[i]+nums[j] == 2020 {
                return (nums[i], nums[j]);
            }
        }
    }
    return (0, 0);
}

fn main() -> std::io::Result<()> {
    let mut file = std::fs::File::open("input")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let nums = split_string(&contents);
    let (a, b) = find_nums(&nums);
    let result = a*b;
    println!("{}", result);
    return Ok(());
}
