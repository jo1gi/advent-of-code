use std::collections::HashMap;

fn count_arrengements<'a>(adapters: &'a [u32], results: &mut HashMap<&'a [u32], u64>) -> u64 {
    if adapters.len() == 1 {
        return 1;
    }
    if results.contains_key(adapters) {
        return *results.get(adapters).unwrap();
    }
    let mut count = 0;
    for i in 1..adapters.len() {
        let x = adapters[i] - adapters[0];
        if x <= 3 {
            count += count_arrengements(&adapters[i..], results);
        } else {
            break;
        }
    }
    results.insert(adapters, count);
    return count;
}

fn main() {
    let mut adapters: Vec<u32> = std::fs::read_to_string("input")
        .unwrap()
        .lines()
        .filter_map(|x| x.parse::<u32>().ok())
        .collect();
    adapters.sort();
    adapters.insert(0, 0);
    let mut results = HashMap::new();
    let result = count_arrengements(&adapters, &mut results);
    println!("{}", result);
}