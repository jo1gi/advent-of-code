fn main() {
    let mut adapters: Vec<u32> = std::fs::read_to_string("input")
        .unwrap()
        .lines()
        .filter_map(|x| x.parse::<u32>().ok())
        .collect();
    adapters.sort();
    let mut ones = 0;
    let mut threes = 1;
    let mut last = 0;
    for i in adapters {
        let x = i - last;
        match x {
            1 => ones += 1,
            3 => threes += 1,
            _ => (),
        }
        last = i;
    }
    let result = ones * threes;
    println!("{}", result);
}