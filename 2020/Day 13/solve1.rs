fn parse_input(input: &str) -> (i32, Vec<i32>) {
    let mut lines = input.lines();
    let timestamp = lines.next().unwrap().parse::<i32>().unwrap();
    let busses = lines.next().unwrap().split(",").filter_map(|x| x.parse::<i32>().ok()).collect::<Vec<i32>>();
    return (timestamp, busses);
}

fn main() {
    let input = std::fs::read_to_string("input").unwrap();
    let (timestamp, busses) = parse_input(&input);
    let mut departs: Vec<(i32, i32)> = busses.iter().map(|x| {
        let mut count = 0;
        loop {
            count += x;
            if count >= timestamp {
                return (*x, count);
            }
        }
        
    }).collect();
    departs.sort_by_key(|x| x.1);
    let fastest = departs[0];
    let result = fastest.0 * (fastest.1 - timestamp);
    println!("{}", result);
}