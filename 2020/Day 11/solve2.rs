fn count_occupied<'a>(seats: &'a [Vec<char>], row: usize, col: usize) -> u8 {
    let mut count = 0;
    let row_len = seats.len() as i32;
    let col_len = seats[0].len() as i32;
    for i in 0..3 {
        for j in 0..3 {
            let (x, y): (i32, i32) = (-1 + i, -1 + j);
            let (mut r, mut c) = (row as i32, col as i32);
             loop {
                if (x == 0 && y == 0) || (r == 0 && x == -1) || (c == 0 && y == -1) || (r+1 == row_len && x == 1) || (c+1 == col_len && y == 1) {
                    break;
                }
                r += x;
                c += y;
                let cur = seats[r as usize][c as usize];
                match cur {
                    '#' => {
                        count += 1;
                        break;
                    },
                    'L' => break,
                    _ => (),
                }
            }
        }
    }
    return count;
}

fn run_round<'a>(seats: &'a [Vec<char>]) -> Vec<Vec<char>> {
    let mut new_seats = Vec::new();
    let col_len = seats[0].len();
    for row in 0..seats.len() {
        let mut row_vec = Vec::new();
        for col in 0..col_len {
            match seats[row][col] {
                '#' => {
                    if count_occupied(seats, row, col) >= 5 {
                        row_vec.push('L');
                    } else {
                        row_vec.push('#');
                    }
                },
                'L' => {
                    if count_occupied(seats, row, col) == 0 {
                        row_vec.push('#');
                    } else {
                        row_vec.push('L');
                    }
                },
                x => row_vec.push(x),
            }
        }
        new_seats.push(row_vec);
    }
    return new_seats;
}

fn main() {
    let mut seats = std::fs::read_to_string("input")
        .unwrap()
        .lines()
        .map(|x| x.chars().collect::<Vec<char>>())
        .collect::<Vec<Vec<char>>>();
    loop {
        let new = run_round(&seats);
        if new == seats {
            break;
        }
        seats = new;
    }
    println!("{}", seats.iter().map(|x| x.iter().filter(|y| y==&&'#').count()).sum::<usize>());
}
