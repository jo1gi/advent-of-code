use std::io::prelude::*;

fn split_passports(s: &str) -> Vec<String> {
    let mut passports = Vec::new();
    let mut current = String::new();
    for l in s.split("\n") {
        if l == "" {
            passports.push(current);
            current = String::new();
        } else {
            current.push(' ');
            current.push_str(l);
        }
    }
    return passports;
}

fn get_fields<'a>(passport: &'a str) -> Vec<(&'a str, &'a str)> {
    let mut fields = Vec::new();
    for i in passport.split(" ") {
        if i == "" {
            continue;
        }
        let mut field = i.split(":");
        let key = field.next().unwrap();
        let value = field.next().unwrap();
        fields.push((key, value));
    }
    return fields;
}

fn check_len(fields: &[(&str, &str)]) -> bool {
    match fields.len() {
        7 => {
            for i in fields {
                if i.0 == "cid" {
                    return false;
                }
            }
            return true;
        },
        8 => return true,
        _ => return false,
    }
}

fn validate_year(year: &str, field: &str) -> bool {
    let y = match year.parse::<i32>() {
        Ok(y) => y,
        Err(_) => return false,
    };
    let (min, max) = match field {
        "byr" => (1920, 2002),
        "iyr" => (2010, 2020),
        "eyr" => (2020, 2030),
        _ => return false,
    };
    return min <= y && y <= max;
}

fn check_hair(color: &str) -> bool {
    if color.len() != 7 {
        return false;
    }
    let mut c = color.chars();
    if c.next().unwrap() != '#' {
        return false;
    }
    for i in c {
        let mut m = false;
        for j in "1234567890abcdef".chars() {
            if i == j {
                m = true;
            }
        }
        if !m {
            return false;
        }
    }
    return true;
}

fn validate_eye(eye: &str) -> bool {
    for j in &["amb", "blu", "brn", "gry", "grn", "hzl", "oth"] {
        if &eye == j {
            return true;
        }
    }
    return false;
}

fn validate_height(height: &str) -> bool {
    let mut num = String::new();
    let mut min = 0;
    let mut max = 0;
    for i in height.chars() {
        if i.is_numeric() {
            num.push(i);
        } else {
            match i {
                'c' => {
                    min = 150;
                    max = 193;
                },
                'i' => {
                    min = 59;
                    max = 76;
                },
                _ => return false,
            }
            break;
        }
    }
    let h = num.parse::<i32>().unwrap();
    return min <= h && h <= max;
}

fn validate_passport(passport: &str) -> bool {
    let fields = get_fields(passport);
    if !check_len(&fields) {
        return false;
    }
    for i in fields {
        let a = match i.0 {
            "hgt" => validate_height(i.1),
            "byr"|"iyr"|"eyr" => validate_year(i.1, i.0),
            "hcl" => check_hair(i.1),
            "ecl" => validate_eye(i.1),
            "pid" => i.1.len() == 9,
            _ => true,
        };
        if !a {
            return false;
        }
    }
    return true;
}

fn main() -> std::io::Result<()> {
    let mut file = std::fs::File::open("input")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let passports = split_passports(&contents);
    let mut count = 0;
    for i in passports {
        if validate_passport(&i) {
            count += 1;
        }
    }
    println!("{}", count);
    Ok(())
}
