use std::io::prelude::*;

fn main() -> std::io::Result<()> {
    let mut file = std::fs::File::open("input")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;

    let forrest: Vec<&str> = contents.split("\n").collect();
    let width = forrest[0].len();
    let slope: (usize, usize) = (3, 1);
    let mut pos: (usize, usize) = (0, 0);
    let mut count = 0;
    loop {
        if pos.1 >= forrest.len() || forrest[pos.1] == "" {
            break;
        }
        let x = pos.0 % width;
        let current = &forrest[pos.1][x..x+1];
        if current == "#" {
            count += 1;
        }
        pos.0 += slope.0;
        pos.1 += slope.1;
    }
    println!("Trees: {}", count);
    Ok(())
}
