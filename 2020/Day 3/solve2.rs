use std::io::prelude::*;

fn count_trees(forrest: &Vec<&str>, slope: &(usize, usize)) -> i32 {
    let width = forrest[0].len();
    let mut pos: (usize, usize) = (0, 0);
    let mut count = 0;
    loop {
        if pos.1 >= forrest.len() || forrest[pos.1] == "" {
            break;
        }
        let x = pos.0 % width;
        let current = &forrest[pos.1][x..x+1];
        if current == "#" {
            count += 1;
        }
        pos.0 += slope.0;
        pos.1 += slope.1;
    }
    return count;
}

fn main() -> std::io::Result<()> {
    let mut file = std::fs::File::open("input")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;

    let forrest: Vec<&str> = contents.split("\n").collect();
    let mut result = 1;
    for slope in [(1, 1), (3,1), (5,1), (7,1), (1,2)].iter() {
        result *= count_trees(&forrest, slope);
    }
    println!("Trees: {}", result);
    Ok(())
}
