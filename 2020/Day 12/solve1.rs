fn parse_direction(line: &str) -> Option<(char, i32)> {
    if line == "" {
        return None;
    }
    let direction = line.chars().next().unwrap();
    let distance = match line[1..].parse::<i32>() {
        Ok(x) => x,
        Err(_) => return None,
    };
    return Some((direction, distance));
}

fn calc_distance(directions: &[(char, i32)]) -> i32 {
    let mut facing = 0;
    let mut lat = 0;
    let mut long = 0;
    for d in directions {
        match d.0 {
            'F' => {
                match facing {
                    0 => lat += d.1, // East
                    90 => long -= d.1, // South
                    180 => lat -= d.1, // West
                    270 => long += d.1, // North
                    x => println!("{}", x),
                }
            },
            'R' => {
                facing = (facing + d.1) % 360;
            },
            'L' => {
                facing = (facing + (360 - d.1)) % 360;
            }
            'E' => lat += d.1,
            'N' => long += d.1,
            'W' => lat -= d.1,
            'S' => long -= d.1,
            _ => (),
        }
    }
    println!("{}; {}", lat, long);
    return lat.abs() + long.abs();
}

fn main() {
    let directions = std::fs::read_to_string("input")
        .unwrap()
        .lines()
        .filter_map(parse_direction)
        .collect::<Vec<(char, i32)>>();
    let distance = calc_distance(&directions);
    println!("{}", distance);
}
