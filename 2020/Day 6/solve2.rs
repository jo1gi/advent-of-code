use std::io::prelude::*;

const QUESTIONS: usize = 26;

fn combine_group(group: &str) -> [bool; 26] {
    let members = group.lines().count();
    let mut answers: [u8; QUESTIONS] = Default::default();
    for line in group.lines() {
        for c in line.chars() {
            answers[((c as u32) - 97) as usize] += 1;
        }
    }
    let mut result: [bool; QUESTIONS] = Default::default();
    for i in 0..QUESTIONS {
        if answers[i] == members as u8 {
            result[i] = true;
        }
    }
    return result;
}

fn count_answers(answers: [bool; 26]) -> i32 {
    let mut count = 0;
    for a in answers.iter() {
        if *a {
            count += 1;
        }
    }
    return count;
}

fn main() -> std::io::Result<()> {
    let mut file = std::fs::File::open("input")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;

    let sum: i32 = contents.split("\n\n")
        .map(combine_group)
        .map(count_answers)
        .sum();
    println!("{}", sum);
    Ok(())
}
