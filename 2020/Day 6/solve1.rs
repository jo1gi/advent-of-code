use std::io::prelude::*;

fn combine_group(group: &str) -> [bool; 26] {
    let mut answers: [bool; 26] = Default::default();
    for line in group.lines() {
        for c in line.chars() {
            answers[((c as u32) - 97) as usize] = true;
        }
    }
    return answers;
}

fn count_answers(answers: [bool; 26]) -> i32 {
    let mut count = 0;
    for a in answers.iter() {
        if *a {
            count += 1;
        }
    }
    return count;
}

fn main() -> std::io::Result<()> {
    let mut file = std::fs::File::open("input")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;

    let sum: i32 = contents.split("\n\n")
        .map(combine_group)
        .map(count_answers)
        .sum();
    println!("{}", sum);
    Ok(())
}
