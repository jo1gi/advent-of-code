fn is_valid(numbers: &[u64], pst: usize) -> bool {
    let mut pre = numbers[pst-25..pst].to_vec();
    let num = numbers[pst] as u64;
    pre.sort();
    let (mut start, mut end) = (0, 24);
    while start != end {
        let combined = pre[start] + pre[end];
        if combined == num {
            return true;
        }
        if combined < num {
            start += 1;
        } else if combined > num {
            end -= 1;
        }
    }
    return false;
}

fn find_invalid(numbers: &[u64]) -> u64 {
    for i in 25..numbers.len() {
        if !is_valid(numbers, i) {
            return numbers[i];
        }
    }
    return 0;
}

fn find_parts_of_invalid(numbers: &[u64], invalid: u64) -> &[u64] {
    for i in 0..numbers.len() {
        let mut count = 0;
        let mut sum = 0;
        while sum < invalid {
            sum += numbers[i+count];
            if sum == invalid {
                return &numbers[i..i+count+1];
            }
            count += 1;
        }
    }
    return &[0];
}

fn main() {
    let numbers: Vec<u64> = std::fs::read_to_string("input")
        .unwrap()
        .lines()
        .filter_map(|x| x.parse::<u64>().ok())
        .collect();
    let invalid = find_invalid(&numbers);
    let parts = find_parts_of_invalid(&numbers, invalid);
    let min = parts.iter().min().unwrap();
    let max = parts.iter().max().unwrap();
    let result = min+max;
    println!("{}", result);
}
