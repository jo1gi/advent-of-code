fn is_valid(numbers: &[i64], pst: usize) -> bool {
    let mut pre = numbers[pst-25..pst].to_vec();
    let num = numbers[pst] as i64;
    pre.sort();
    let (mut start, mut end) = (0, 24);
    while start != end {
        let combined = pre[start] + pre[end];
        if combined == num {
            return true;
        }
        if combined < num {
            start += 1;
        } else if combined > num {
            end -= 1;
        }
    }
    return false;
}

fn find_invalid(numbers: &[i64]) -> i64 {
    for i in 25..numbers.len() {
        if !is_valid(numbers, i) {
            return numbers[i];
        }
    }
    return 0;
}

fn main() {
    let numbers: Vec<i64> = std::fs::read_to_string("input")
        .unwrap()
        .lines()
        .filter_map(|x| x.parse::<i64>().ok())
        .collect();
    let invalid = find_invalid(&numbers);
    println!("{}", invalid);
}
