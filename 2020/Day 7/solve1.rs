#[derive(Clone, Debug)]
struct Rule {
    container: String,
    amount: u8,
    bag: String,
}

fn parse_rules(line: &str) -> Vec<Rule> {
    let mut split = line.split(" bags contain ");
    let container = split.next().unwrap();
    let mut rules = Vec::new();
    for i in split.next().unwrap().split(", ") {
        if i[0..2] == *"no" {
            continue;
        }
        let start = i.find(' ').unwrap();
        let amount = i[0..start].parse::<u8>().unwrap();
        let mut bag = i[start+1..].to_string();
        for i in &[" bags", " bag", "."] {
            bag = bag.replace(i, "");
        }
        rules.push(Rule {
            container: container.to_string(),
            amount: amount,
            bag: bag.to_string(),
        });
    }
    return rules;
}

fn main() {
    let input = std::fs::read_to_string("input").unwrap();
    let mut rules = Vec::new();
    for i in input.lines() {
        let mut rule_set = parse_rules(i);
        rules.append(&mut rule_set);
    }
    let mut bags = vec!["shiny gold"];
    let mut last = 0;
    while last != bags.len() {
        last = bags.len();
        let mut new: Vec<&str> = rules.iter()
            .filter(|r| bags.contains(&r.bag.as_str()))
            .filter(|r| !bags.contains(&r.container.as_str()))
            .map(|r| r.container.as_str())
            .collect();
        bags.append(&mut new);
    }
    bags.dedup();
    println!("{:?}", bags.len()-1);
}
