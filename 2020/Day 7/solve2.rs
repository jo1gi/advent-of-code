use std::collections::HashMap;

#[derive(Clone, Debug)]
struct Rule {
    container: String,
    bags: Vec<(String, u8)>,
}

fn parse_rules(line: &str) -> Rule {
    let mut split = line.split(" bags contain ");
    let container = split.next().unwrap();
    let mut bags = Vec::new();
    for i in split.next().unwrap().split(", ") {
        if i[0..2] == *"no" {
            continue;
        }
        let start = i.find(' ').unwrap();
        let amount = i[0..start].parse::<u8>().unwrap();
        let mut bag = i[start+1..].to_string();
        for i in &[" bags", " bag", "."] {
            bag = bag.replace(i, "");
        }
        bags.push((bag, amount));
    }
    return Rule {
        container: container.to_string(),
        bags: bags,
    };
}

fn count_bags(map: &HashMap<String, Vec<(String, u8)>>, bag: &str) -> u32 {
    if !map.contains_key(bag) {
        return 1;
    }
    let mut count: u32 = 1;
    map.get(bag)
        .unwrap()
        .iter()
        .for_each(|x| {
            count += x.1 as u32 * count_bags(map, &x.0);
        });
    return count;
}

fn main() {
    let input = std::fs::read_to_string("input").unwrap();
    let mut map = HashMap::new();
    input.lines()
        .map(parse_rules)
        .for_each(|x| {map.insert(x.container, x.bags);});
    let bags = count_bags(&map, "shiny gold")-1;
    println!("{}", bags);
}
