use std::io::prelude::*;

struct Password {
    min: i32,
    max: i32,
    letter: char,
    password: String,
}

fn parse_line(s: &str) -> Password {
    let parts: Vec<&str> = s.split(" ").collect();
    let limits: Vec<i32> = parts[0]
                     .split("-")
                     .map(|x| x.parse::<i32>().unwrap())
                     .collect();
    let letter = parts[1].chars().next().unwrap();
    Password {
        min: limits[0],
        max: limits[1],
        letter: letter,
        password: parts[2].to_string()
    }
}

fn is_valid(s: &str) -> bool {
    if s == "" {
        return false;
    }
    let pass = parse_line(s);
    let mut count = 0;
    for i in pass.password.chars() {
        if i == pass.letter {
            count += 1;
        }
    }
    if pass.min <= count && count <= pass.max {
        return true;
    }
    return false;
}

fn main() -> std::io::Result<()> {
    let mut file = std::fs::File::open("input")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let mut count = 0;
    for line in contents.split("\n") {
        if is_valid(line) {
            count += 1;
        }
    }
    println!("Valid passwords: {}", count);
    Ok(())
}
