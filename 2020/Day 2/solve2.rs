use std::io::prelude::*;

struct Password {
    min: usize,
    max: usize,
    letter: char,
    password: String,
}

fn parse_line(s: &str) -> Password {
    let parts: Vec<&str> = s.split(" ").collect();
    let limits: Vec<usize> = parts[0]
                     .split("-")
                     .map(|x| x.parse::<usize>().unwrap())
                     .collect();
    let letter = parts[1].chars().next().unwrap();
    Password {
        min: limits[0],
        max: limits[1],
        letter: letter,
        password: parts[2].to_string()
    }
}

fn is_valid(s: &str) -> bool {
    if s == "" {
        return false;
    }
    let pass = parse_line(s);
    let chars: Vec<char> = pass.password.chars().collect();
    let mut o = false;
    for i in &[pass.min, pass.max] {
        if chars[i-1] == pass.letter {
            o = !o;
        }
    }
    return o;
}

fn main() -> std::io::Result<()> {
    let mut file = std::fs::File::open("input")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let mut count = 0;
    for line in contents.split("\n") {
        if is_valid(line) {
            count += 1;
        }
    }
    println!("Valid passwords: {}", count);
    Ok(())
}
