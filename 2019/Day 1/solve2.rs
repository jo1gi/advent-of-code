use std::io::prelude::*;

fn get_fuel(mass: f64) -> f64 {
    let fuel = (mass/3.0).floor()-2.0;
    if fuel < 0.0 {
        return 0.0;
    }
    return fuel + get_fuel(fuel);
}

fn main() -> std::io::Result<()> {
    let mut file = std::fs::File::open("input")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let mut total: f64 = 0.0;
    for mass in contents.split("\n") {
        let num = {
            match mass.parse() {
                Ok(num) => num,
                Err(_) => continue,
            }
        };
        total = total + get_fuel(num);
    }
    println!("{}", total);
    Ok(())
}
