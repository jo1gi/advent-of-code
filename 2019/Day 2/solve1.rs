use std::io::prelude::*;

fn get_value(nums: &[i32], pos: usize) -> i32 {
    nums[nums[pos] as usize]
}

fn update<F: Fn(i32, i32) -> i32>(nums: &mut [i32], pos: usize, operation: F) {
    let val1 = get_value(&nums, pos+1);
    let val2 = get_value(&nums, pos+2);
    let location = nums[pos+3] as usize;
    nums[location] = operation(val1, val2);
}

fn main() -> std::io::Result<()> {
    let mut file = std::fs::File::open("input")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let mut nums: Vec<i32> = contents.split(",")
        .map(|x| x.replace("\n", ""))
        .map(|x| x.parse::<i32>().unwrap())
        .collect();
    nums[1] = 12;
    nums[2] = 2;
    let mut pos = 0;
    loop {
        match nums[pos] {
            1 => update(&mut nums, pos, |x, y| {x + y}),
            2 => update(&mut nums, pos, |x, y| {x * y}),
            99 => {
                break;
            }
            _ => {
                println!("Unexptected Code");
                break;
            }
        }
        pos += 4;
    }
    println!("{}", nums[0]);
    Ok(())
}
