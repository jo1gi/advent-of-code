(require '[clojure.string :as string])

(defn to-bit-array [input]
  (->> (string/replace input #"\n" "")
       (map #(BigInteger. (str %) 16))
       (map #(.toString % 2))
       (map #(str (apply str (repeat (- 4 (count %)) 0)) %))
       (apply str)))

(defn bits-to-int [bits]
  (-> bits
      (BigInteger. 2)))

(defn parse-literal [bits]
  (loop [partitions (vec (partition 5 bits))
         contents ""]
    (let [current (first partitions)
          value (string/join "" (rest current))
          newcontents (str contents value)]
      (if (= (first current) \1)
        (recur (rest partitions) newcontents)
        [(bits-to-int newcontents) (* (/ (count newcontents) 4) 5)]))))

(defn operation-bits [bits]
  (let [length (bits-to-int (subs bits 1 16))]
    (update
      (parse-packets (subs bits 16 (+ length 16)) nil)
      1 + 16)))

(defn operation-subpackets [bits]
  (let [subpackets (bits-to-int (subs bits 1 12))]
    (update
      (parse-packets (subs bits 12) subpackets)
      1 + 12)))

(defn parse-operation [bits]
  (if (= (first bits) \0)
    (operation-bits bits)
    (operation-subpackets bits)))

(defn parse-packet [bits]
  (let [version (bits-to-int (subs bits 0 3))
        typeid (bits-to-int (subs bits 3 6))
        [contents size] ((if (= typeid 4)
              parse-literal
              parse-operation)
            (subs bits 6))]
    [{:version version
      :typeid typeid
      :contents contents}
     (+ size 6)]
    ))

(defn parse-packets [bits n]
  (loop [bits bits
         packets []
         packetssize 0]
    (let [[packet length] (parse-packet bits)
          newpackets (conj packets packet)
          newsize (+ packetssize length)
          restbits (subs bits length)]
      (if (and (> (count restbits) 10)
               (or (nil? n) (< (count newpackets) n)))
        (recur restbits newpackets newsize)
        [newpackets newsize])
      )))

(defn comparesubpackets [subpackets comparitor]
  (let [v (vec subpackets)
        a (get v 0)
        b (get v 1)]
    (if (comparitor a b)
      1 0)))

(defn calculate-packet [packet]
  (let [contents (packet :contents)
        subpackets (map calculate-packet contents)]
    (case (packet :typeid)
      0 (reduce + subpackets)
      1 (reduce * subpackets)
      2 (apply min subpackets)
      3 (apply max subpackets)
      4 contents
      5 (comparesubpackets subpackets >)
      6 (comparesubpackets subpackets <)
      7 (comparesubpackets subpackets =)
      )))

(->> (slurp "./input.txt")
     (to-bit-array)
     (parse-packet)
     (#(get % 0))
     (calculate-packet))
