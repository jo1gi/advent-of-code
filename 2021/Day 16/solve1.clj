(require '[clojure.string :as string])

(defn to-bit-array [input]
  (->> (string/replace input #"\n" "")
       (map #(BigInteger. (str %) 16))
       (map #(.toString % 2))
       (map #(str (apply str (repeat (- 4 (count %)) 0)) %))
       (apply str)))

(defn bits-to-int [bits]
  (-> bits
      (BigInteger. 2)))

(defn parse-literal [bits]
  (loop [partitions (vec (partition 5 bits))
         contents ""]
    (let [current (first partitions)
          value (string/join "" (rest current))
          newcontents (str contents value)]
      (if (= (first current) \1)
        (recur (rest partitions) newcontents)
        [(bits-to-int newcontents) (* (/ (count newcontents) 4) 5)]))))

(defn parse-operation [bits]
  (let [sizelength (if (= (first bits) \0) 15 11)
        subpackets (bits-to-int (subs bits 1 (+ sizelength 1)))
        packets (-> bits
                    (subs (+ sizelength 1))
                    (parse-packets subpackets)
                    (get 0)
                    ; (->> (take subpackets))
                    )
        size (reduce + (map #(get % :size) packets))]
    [packets (+ size sizelength 1)]))

(defn parse-packet [bits]
  (let [version (bits-to-int (subs bits 0 3))
        typeid (bits-to-int (subs bits 3 6))
        [contents size] ((if (= typeid 4)
              parse-literal
              parse-operation)
            (subs bits 6))]
    {:version version
      :typeid typeid
      :contents contents
      :size (+ size 6)}
    ))

(defn find-version-numbers [packet]
  (if (not (= (packet :typeid) 4))
    (->> (packet :contents)
         (map find-version-numbers)
         (apply concat)
         (#(conj % (packet :version))))
    [(packet :version)]))

(defn find-version-sum [packet]
  (->> packet
       (find-version-numbers)
       (reduce +)))

(parse-packet "00111000000000000110111101000101001010010001001000000000")
(find-version-sum (parse-packet "11101110000000001101010000001100100000100011000001100000"))

(defn parse-packets [bits n]
  (loop [bits bits
         packets []
         packetssize 0]
    (let [packet (parse-packet bits)
          length (packet :size)
          newpackets (conj packets packet)
          newsize (+ packetssize length)
          restbits (subs bits length)]
      (if (and (> (count restbits) 10)
               (or (< (count newpackets) n) (nil? n)))
        (recur restbits newpackets newsize)
        [newpackets newsize])
      )))


(->> (slurp "input.txt")
  (to-bit-array)
  (parse-packet)
  (find-version-sum)
  )
