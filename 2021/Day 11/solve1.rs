type Grid = Vec<Vec<u32>>;

fn create_row(line: &str) -> Vec<u32> {
    line.chars()
        .map(|x| x.to_digit(10).unwrap())
        .collect::<Vec<u32>>()
}

fn find_flashing(grid: &mut Grid) -> Vec<(usize, usize)> {
    let mut queue = Vec::new();
    for y in 0..grid.len() {
        for x in 0..grid[0].len() {
            grid[y][x] += 1;
            if grid[y][x] > 9 {
                queue.push((y, x,));
            }
        }
    }
    return queue;
}

fn run_round(grid: &mut Grid) -> u32 {
    let mut counter = 0;
    let mut queue = find_flashing(grid);
    while !queue.is_empty() {
        counter += 1;
        let (y, x) = queue.pop().unwrap();
        grid[y][x] = 0;
        for i in 0..3 {
            for j in 0..3 {
                let a = y+i;
                let b = x+j;
                if a > grid.len() || a == 0 || b > grid[0].len() || b == 0 {
                    continue;
                }
                let (a, b) = (a-1, b-1);
                if grid[a][b] > 0 && grid[a][b] < 10 {
                    grid[a][b] += 1;
                    if grid[a][b] == 10 {
                        queue.push((a, b,));
                    }
                }
            }
        }
    }
    return counter;
}

fn main() {
    let mut grid = std::fs::read_to_string("input.txt")
        .unwrap()
        .lines()
        .map(create_row)
        .collect::<Grid>();
    let mut c = 0;
    for _i in 0..100 {
        c += run_round(&mut grid);
    }
    println!("{}", c);
}
