(require '[clojure.string :as string])


(defn line-to-row [line]
  (vec (map (fn [x] (Character/digit x 10)) line)))

(defn input-to-grid [input]
  (->> (string/split input #"\n")
       (map line-to-row)
       (vec)))

(defn get-value [grid x y]
  (+
   (get (get grid y) x)
  (reduce (count (filter some? (adjecent-flashes grid x y))))))

(defn flashing? [grid x y]
  (>= grid (get-value grid x y) 9))

(defn adjecent [height width a b]
  (->> [[0 -1] [0 1] [-1 0] [1 0]]
       (map (fn [[y x]] [(+ y a) (+ x b)]))
       (filter
         (fn [[y x]] (and (>= x 0) (>= y 0) (< x width) (< y height))))))

(defn adjecent-flashes [grid x y]
  (->> (adjecent (count grid) (count (get grid 0)) x y)
       (map #(flashing? grid x y))
       (count)
       ))

(defn count-flashes [acc grid x y])

(defn count-flashes-row [acc grid x]
  (->>
    (map #(+ (adjecent-flashes) get-value ))))

(defn oct-flashes [grid]
  (loop [grid grid
         i 0
         c 0]
    (if (< i 100)
      (recur
        grid
        (inc i)
        (reduce (fn [acc x] (count-flashes-row acc grid x)) c grid))
      c)
    ))

(->> (slurp "./test.txt")
     (input-to-grid)
     (oct-flashes))
