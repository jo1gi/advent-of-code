(require '[clojure.string :as string])

(defn adjacent [height width a b]
  (->> [[0 -1] [0 1] [-1 0] [1 0]]
       (map (fn [[y x]] [(+ y a) (+ x b)]))
       (filter
         (fn [[y x]] (and (>= x 0) (>= y 0) (< x width) (< y height))))))

(defn line-to-row [line]
  (vec (map #(Character/digit % 10) line)))

(defn input-to-grid [input]
  (->> (string/split input #"\n")
       (map line-to-row)
       (vec)))

(defn grid-get-value [grid y x]
  (get (get grid y) x))

(defn is-low-point? [grid height width [a b]]
  (->> (adjacent height width a b)
       (map (fn [[y x]] (grid-get-value grid y x)))
       (filter #(<= % (grid-get-value grid a b)))
       (count)
       (= 0)
       ))

(defn grid-size [grid]
  {:height (count grid)
   :width (count (get grid 0))})

(defn find-low-points [grid]
  (let [s (grid-size grid)
        height (s :height)
        width (s :width)]
    (->>
      (for [y (range 0 height)
            x (range 0 width)]
        [y x])
      (filter (fn [x] (is-low-point? grid height width x))))))

(defn find-basin [grid [a b]]
  (let [size (grid-size grid)
        pvalue (grid-get-value grid a b)]
    (->> (adjacent (size :height) (size :width) a b)
         (filter (fn [[y x]] (let [v (grid-get-value grid y x)] (and (> v pvalue) (< v 9)))))
         (map #(concat [%] (find-basin grid %)))
         (apply concat)
         (distinct))))

(let [grid (input-to-grid (slurp "./input.txt"))]
  (->> (find-low-points grid)
       (map #(find-basin grid %))
       (map count)
       (map inc)
       (sort)
       (reverse)
       (take 3)
       (reduce *)
       ))
