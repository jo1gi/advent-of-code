(require '[clojure.string :as string])

(defn line-to-row [line]
  (vec (map (fn [x] (Character/digit x 10)) line)))

(defn input-to-grid [input]
  (->> (string/split input #"\n")
       (map line-to-row)
       (vec)))

(defn grid-get-value [grid y x]
  (get (get grid y) x))

(defn is-low-point? [grid height width [a b]]
  (->> [[0 -1] [0 1] [-1 0] [1 0]]
       (map (fn [[y x]] [(+ y a) (+ x b)]))
       (filter
         (fn [[y x]] (and (>= x 0) (>= y 0) (< x width) (< y height))))
       (map (fn [[y x]] (grid-get-value grid y x)))
       (filter (fn [value] (<= value (grid-get-value grid a b))))
       (count)
       (= 0)
       ))

(defn find-low-points [grid]
  (let [height (count grid)
        width (count (get grid 0))]
    (->>
      (for [y (range 0 height)
            x (range 0 width)]
        [y x])
      (filter (fn [x] (is-low-point? grid height width x))))))

(let [grid (input-to-grid (slurp "./input.txt"))]
  (->> (find-low-points grid)
       (map (fn [[y x]] (grid-get-value grid y x)))
       (map inc)
       (reduce +)
       ))
