(require '[clojure.string :as string])

(defn parse-input [input]
  (map (fn [x] (Integer. x))
       (string/split (string/replace input "\n" "") #",")))

(defn find-best-position [crabs]
  (get (vec(sort crabs)) (Math/round (double (/ (count crabs) 2)))))

(defn calc-fuel [crabs position]
  (reduce (fn [x crab] (+ x (Math/abs (- position crab)))) 0 crabs))

(let [crabs (parse-input (slurp "./input.txt"))
      pos (find-best-position crabs)]
  (println (calc-fuel crabs pos)))
