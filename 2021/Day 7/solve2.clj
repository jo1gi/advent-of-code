(require '[clojure.string :as string])

(defn parse-input [input]
  (map (fn [x] (Integer. x))
       (string/split (string/replace input "\n" "") #",")))

(defn find-best-position [crabs]
  (Math/floor (double (/ (reduce + crabs) (count crabs)))))

(defn calc-fuel [crabs position]
  (reduce
    (fn [x crab]
      (+ x (reduce + (range 1 (+ 1 (Math/abs (- position crab)))))))
    0 crabs))

(let [crabs (parse-input (slurp "./input.txt"))
      pos (find-best-position crabs)]
  (println (calc-fuel crabs pos)))
