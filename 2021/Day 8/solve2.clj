(require '[clojure.string :as string])
(require '[clojure.set :as set])

(defn get-output [input]
    (string/split (get (string/split input #" \| ") 1) #" "))

(defn include-chars? [cs s]
  (reduce
    (fn [prev c] (and prev (string/includes? s (str c))))
    true
    cs))

(defn segment-to-number [m code]
  (let [len (count code)]
    (cond (= len 2) 1
          (= len 4) 4
          (= len 3) 7
          (= len 7) 8
          (= len 5) (cond
                      (include-chars? (m :1) code) 3
                      (include-chars? (set/difference (m :4) (m :1)) code) 5
                      :else 2)
          (= len 6) (cond
                      (include-chars? (m :4) code) 9
                      (include-chars? (m :1) code) 0
                      :else 6))))

(defn find-known-numbers [codes]
  (reduce
    (fn [known code]
      (let [len (count code)
            y (cond (= len 2) :1
                    (= len 4) :4
                    (= len 3) :7
                    (= len 7) :8)]
        (if (some? y) (assoc known y (set code)) known)))
    {}
    codes))

(defn find-known-numbers-line [line]
  (find-known-numbers (->> line
       ((fn [x] (string/split x #" ")))
       (filter (fn [x] (not= "|" x))))))

(defn display-to-number [known codes]
  (reduce
    (fn [x y] (+ (* x 10) y) )
    0
    (map (fn [x] (segment-to-number known x)) codes)))

(defn line-to-number [line]
  (display-to-number (find-known-numbers-line line) (get-output line)))

(->> (slurp "./input.txt")
     ((fn [x] (string/split x #"\n")))
     (map line-to-number)
     (reduce +))
