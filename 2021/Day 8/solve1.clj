(use 'clojure.java.io)
(require '[clojure.string :as string])

(defn get-output [input]
    (get (string/split input #" \| ") 1))

(defn segment-to-number [code]
  (let [len (count code)]
    (cond (= len 2) 1
          (= len 3) 7
          (= len 4) 4
          (= len 7) 8)))

(->> (slurp "./input.txt")
     ((fn [x] (string/split x #"\n")))
     (map get-output)
     (map (fn [x] (string/split x #" ")))
     (apply concat)
     (map segment-to-number)
     (filter some?)
     (count)
     )
