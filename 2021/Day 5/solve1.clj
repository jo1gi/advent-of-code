(use 'clojure.java.io)
(require '[clojure.string :as string])

(defn parse-vent [line]
  (let [s (string/split line #" -> ")
        start (string/split (get s 0) #",")
        end (string/split (get s 1) #",")]
    (hash-map :x1 (Integer. (get start 0))
              :y1 (Integer. (get start 1))
              :x2 (Integer. (get end 0))
              :y2 (Integer. (get end 1)))))

(defn filter-diagonal [{x1 :x1 x2 :x2 y1 :y1 y2 :y2}]
  (or (= x1 x2) (= y1 y2)))

(defn vent-to-cells
  ([vent] (vent-to-cells [] vent))
  ([cells {x1 :x1 x2 :x2 y1 :y1 y2 :y2}]
   (let [newcells (conj cells {:x x1 :y y1})
         newx (cond (> x1 x2) (dec x1) (< x1 x2) (inc x1) :else x1)
         newy (cond (> y1 y2) (dec y1) (< y1 y2) (inc y1) :else y1)]
     (if (and (= x1 x2) (= y1 y2)) newcells
       (recur newcells {:x1 newx :x2 x2 :y1 newy :y2 y2})))))

(defn add-cells-to-map [ventmap {y :y x :x}]
  (assoc ventmap y (update-in (get ventmap y) [x] inc)))

(with-open [rdr (reader "./input.txt")]
  (println
    (->>
      (->>
        (line-seq rdr)
        (map parse-vent)
        (filter filter-diagonal)
        (map vent-to-cells))
      (apply concat)
      (reduce
        add-cells-to-map
        (vec (repeat 1000 (vec (repeat 1000 0)))))
      (reduce
        (fn [c row]
          (reduce (fn [d cell] (if (>= cell 2) (inc d) d)) c row))
        0))))
