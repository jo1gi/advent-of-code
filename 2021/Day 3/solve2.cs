using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

class Solve1 {
    static int FindNumber(string[] lines, Func<int, double, bool> lambda) {
        List<string> valid = lines.ToList<string>();
        for(int i = 0; i < 12; i++) {
            if(valid.Count == 1) {
                break;
            }
            int count = 0;
            foreach(string line in valid) {
                if(line[i] == '1') {
                    count++;
                }
            }
            char x = lambda(count, valid.Count/2.0) ? '1' : '0';
            for(int j = valid.Count-1; j >= 0; j--){
                if(valid[j][i] != x) {
                    valid.RemoveAt(j);
                }
            }
        }
        return Convert.ToInt32(valid[0], 2);
    }

    static void Main(string[] args) {
        // Reading file
        string[] lines = System.IO.File.ReadAllLines("./input.txt");
        int oxygen = FindNumber(lines, (count, halfsize) => count >= halfsize);
        int co2 = FindNumber(lines, (count, halfsize) => count < halfsize);
        Console.WriteLine(oxygen*co2);
    }
}
