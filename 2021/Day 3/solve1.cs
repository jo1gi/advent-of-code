using System;

class Solve1 {
    static void Main(string[] args) {
        // Reading file
        string[] lines = System.IO.File.ReadAllLines("./input.txt");
        int[] gamma = new int[12];
        int[] epsilon = new int[12];
        for(int i = 0; i < 12; i++) {
            int count = 0;
            foreach(string line in lines) {
                if(line[i] == '1') {
                    count++;
                }
            }
            if(count > lines.Length/2){
                gamma[i] = 1;
                epsilon[i] = 0;
            } else {
                gamma[i] = 0;
                epsilon[i] = 1;
            }
        }
        int g = Convert.ToInt32(string.Join("", gamma), 2);
        int e = Convert.ToInt32(string.Join("", epsilon), 2);
        Console.WriteLine(g*e);
    }
}
