use std::collections::HashMap;

type CaveMap = HashMap<String, Vec<String>>;
type History = HashMap<String, u32>;

fn create_map(input: String) -> CaveMap {
    let mut cavemap = HashMap::new();
    for line in input.lines() {
        let mut parts = line.split("-");
        let from = parts.next().unwrap().to_string();
        let to = parts.next().unwrap().to_string();
        if !cavemap.contains_key(&from) {
            cavemap.insert(from.clone(), Vec::new());
        }
        cavemap.get_mut(&from).unwrap().push(to.clone());
        if !cavemap.contains_key(&to) {
            cavemap.insert(to.clone(), Vec::new());
        }
        cavemap.get_mut(&to).unwrap().push(from);
    }
    return cavemap;
}

fn is_small(name: &str) -> bool {
    name.find(char::is_lowercase).is_some()
}

fn find_routes(cavemap: &CaveMap, current: &String, prev: &Vec<String>, history: &mut History, small_cave_twice: bool) -> u32 {
    if current == "end" {
        return 1;
    }
    if current == "start" && prev.len() > 0 {
        return 0;
    }
    if is_small(current) && prev.contains(current) && small_cave_twice {
        return 0;
    }
    if is_small(current) && history.contains_key(current) {
        return *history.get(current).unwrap();
    }
    let mut new_prev = prev.clone();
    new_prev.push(current.to_string());
    let mut count = 0;
    for i in cavemap.get(current).unwrap() {
        let twice = small_cave_twice || (is_small(current) && prev.contains(current));
        count += find_routes(cavemap, i, &new_prev, history, twice)
    }
    return count;
}

fn main() {
    let lines = std::fs::read_to_string("input.txt").unwrap();
    let cavemap = create_map(lines);
    let result = find_routes(&cavemap, &"start".to_string(), &Vec::new(), &mut HashMap::new(), false);
    println!("{:?}", result);
}
