(require '[clojure.string :as string])

(defrecord Point [x y value]
  java.lang.Comparable
  (compareTo [this other] (- (:value this) (:value other))))

(defn line-to-row [line]
  (mapv (fn [x] (Character/digit x 10)) line))

(defn input-to-grid [input]
  (->> (string/split input #"\n")
       (mapv line-to-row)))

(defn get-value [grid y x]
  (get (get grid y) x))

(defn adjacent [grid height width a b value]
  (->> [[0 -1] [0 1] [-1 0] [1 0]]
       (map (fn [[y x]] [(+ y a) (+ x b)]))
       (filter
         (fn [[y x]] (and (>= x 0) (>= y 0) (< x width) (< y height))))
       (map (fn [[y x]] (Point. x y (+ (get-value grid y x) value))))))


(defn find-shortest-path [grid]
  (let [q (java.util.PriorityQueue.)
        width (count (get grid 0))
        height (count grid)]
    (.add q (Point. 0 0 0))
    (loop [p (.poll q)
           m #{}]
      (if (and (= (:x p) (- width 1)) (= (:y p) (- height 1)))
        (:value p)
          (let [o {:x (:x p) :y (:y p)}]
            (if (not (contains? m o))
              (loop [n (adjacent grid height width (:y p) (:x p) (:value p))]
                (if (empty? n)
                  ()
                  (do
                    (.add q (first n))
                    (recur (rest n)))))
              ())
            (recur (.poll q) (conj m o))))))
            )

(->> (slurp "./input.txt")
     (input-to-grid)
     (find-shortest-path)
     (println))
