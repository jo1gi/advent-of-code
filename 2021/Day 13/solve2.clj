(require '[clojure.string :as string])

(defn parse-coord [line]
  (->> (string/split line #",")
       (map #(Integer. %))
       (vec)))

(defn line-type [line a]
  (cond (= line "") false
        (= \f (first line)) a
        :else (not a)))

(defn parse-coords [lines]
  (->> lines
       (#(string/split % #"\n"))
       (filter #(line-type % false))
       (map parse-coord)))

(defn parse-instruction [line]
  (let [s (string/split line #"=")]
    {:axis (last (get s 0))
     :index (Integer. (get s 1))}))

(defn parse-instructions [lines]
  (->> lines
       (#(string/split % #"\n"))
       (filter #(line-type % true))
       (map parse-instruction)))

(defn fold-dot [dot inst]
  (let [i (if (= (inst :axis) \x) 0 1)
        index (inst :index)]
    (update-in
      dot [i]
      #(if (> % index)
         (- (* 2 index) %)
         %))))

(defn handle-instruction [dots instruction]
  (->> dots
       (map #(fold-dot % instruction))
       (distinct)))

(defn insert-dot [m [x y]]
  (update-in m [y] #(update-in % [x] (constantly "#"))))

(defn create-map [dots]
  (let [width (+ (apply max (map #(get % 0) dots)) 1)
        height (+ (apply max (map #(get % 1) dots)) 1)]
    (->> dots
       (reduce insert-dot (vec (repeat height (vec (repeat width " ")))))
       (map #(string/join "" %))
       (string/join "\n"))))

(let [f (slurp "input.txt")
      dots (parse-coords f)
      instructions (parse-instructions f)]
  (->> instructions
       (reduce handle-instruction dots)
       (create-map)
       (println)))
