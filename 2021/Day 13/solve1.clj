(require '[clojure.string :as string])

(defn parse-coord [line]
  (->> (string/split line #",")
       (map #(Integer. %))
       (vec)))

(defn line-type [line a]
  (cond (= line "") false
        (= \f (first line)) a
        :else (not a)))

(defn parse-coords [lines]
  (->> lines
       (#(string/split % #"\n"))
       (filter #(line-type % false))
       (map parse-coord)))

(defn parse-instruction [line]
  (let [s (string/split line #"=")]
    {:axis (last (get s 0))
     :index (Integer. (get s 1))}))

(defn parse-instructions [lines]
  (->> lines
       (#(string/split % #"\n"))
       (filter #(line-type % true))
       (map parse-instruction)))

(defn fold-dot [dot inst]
  (let [i (if (= (inst :axis) \x) 0 1)
        index (inst :index)]
    (update-in
      dot [i]
      #(if (> % index)
         (- (* 2 index) %)
         %))))

(defn handle-instruction [dots instruction]
  (->> dots
       (map #(fold-dot % instruction))
       (distinct)))

(let [f (slurp "input.txt")
      dots (parse-coords f)
      instructions (parse-instructions f)]
  (->> dots
       (#(handle-instruction % (first instructions)))
       (count)
       (println)))
