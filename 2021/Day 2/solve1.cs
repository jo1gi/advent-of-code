using System;

class Solve1 {
    static void Main(string[] args) {
        // Reading file
        string[] lines = System.IO.File.ReadAllLines("./input.txt");
        // Calculating depth and horizontal position
        int deep = 0;
        int horizontal = 0;
        for(int i = 0; i < lines.Length; i++){
            string[] parts = lines[i].Split(" ");
            int amount = int.Parse(parts[1]);
            if(parts[0] == "down"){
                deep += amount;
            } else if (parts[0] == "up") {
                deep -= amount;
            } else if (parts[0] == "forward"){
                horizontal += amount;
            }
        }
        Console.WriteLine(deep*horizontal);
    }
}
