(require '[clojure.string :as string])

(def points
  {\) 3
   \] 57
   \} 1197
   \> 25137})

(def close [\) \] \} \>])
(def open [\( \[ \{ \<])

(defn in-close? [line]
  (>= (.indexOf close (first line)) 0))

(defn find-first-error [line]
  (loop [prev []
         line line]
    (cond
         (= (count line) 0) nil
         (in-close? line) (if
            (not= (.indexOf open (last prev)) (.indexOf close (first line)))
            (first line)
            (recur (vec (drop-last prev)) (vec (rest line))))
         :else (recur (vec (conj prev (first line))) (vec (rest line))))))

(->> (slurp "./input.txt")
     (#(string/split % #"\n"))
     (map find-first-error)
     (filter some?)
     (map points)
     (reduce +)
     )
