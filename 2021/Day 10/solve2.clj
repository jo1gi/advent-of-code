(require '[clojure.string :as string])
(require '[clojure.set :as set])

(def points
  {\) 1
   \] 2
   \} 3
   \> 4})

(def matching {
  \) \(
  \] \[
  \} \{
  \> \<
})

(defn is-close? [line]
  (contains? matching (first line)))

(defn find-unclosed [line]
  (loop [prev []
         line line]
    (cond
         (= (count line) 0) prev
         (is-close? line) (if
            (not= (matching (first line)) (last prev)) nil
            (recur (vec (drop-last prev)) (vec (rest line))))
         :else (recur (vec (conj prev (first line))) (vec (rest line))))))

(defn to-closed [opened]
  (map (set/map-invert matching) (reverse opened)))

(defn closed-to-points [closed]
  (reduce (fn [total x] (+ (* total 5) (points x))) 0 closed))

(defn middle [lines]
  (->> lines
       (sort)
       (vec)
       (#(get % (- (int (Math/ceil (/ (count lines) 2))) 1)))
       ))

(->> (slurp "./input.txt")
     (#(string/split % #"\n"))
     (map find-unclosed)
     (filter some?)
     (map to-closed)
     (map closed-to-points)
     (middle)
     )
