using System;

class Solve2 {
    static void Main(string[] args) {
        // Reading file
        string[] lines = System.IO.File.ReadAllLines("./input.txt");
        // Converting to ints
        int[] measurements = new int[lines.Length];
        for(int i = 0; i < lines.Length; i++){
            measurements[i] = int.Parse(lines[i]);
        }
        // Combining measurements
        int[] combined = new int[lines.Length+3];
        for(int i = 0; i < lines.Length; i++) {
            for(int j = 0; j < 3; j++){
                combined[i+j] += measurements[i];
            }
        }
        // Counting increases
        int count = 0;
        int combinedLength = lines.Length - (lines.Length % 3);
        for(int i = 1; i < combinedLength; i++) {
            if(combined[i] > combined[i-1]){
                count++;
            }
        }
        Console.WriteLine(count);
    }
}
