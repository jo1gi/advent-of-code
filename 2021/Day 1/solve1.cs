using System;

class Solve1 {
    static void Main(string[] args) {
        // Reading file
        string[] lines = System.IO.File.ReadAllLines("./input.txt");
        // Converting to ints
        int[] measurements = new int[lines.Length];
        for(int i = 0; i < lines.Lenght; i++){
            measurements[i] = int.Parse(lines[i]);
        }
        // Counting increases
        int count = 0;
        for(int i = 1; i < lines.Length; i++) {
            if(measurements[i] > measurements[i-1]){
                count++;
            }
        }
        Console.WriteLine(count);
    }
}
