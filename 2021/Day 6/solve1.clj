(require '[clojure.string :as string])

(defn parse-input [input]
  (->> (string/split (string/replace input "\n" "" ) #",")
       (map (fn[x] (Integer. x)))
       (reduce
         (fn [v x] (update-in v [x] inc))
         (vec (repeat 9 0)))))

(defn simulate-fish [input times]
  (loop [fish input c 0]
    (if (< c times)
      (recur
        (vec (for [i (range 0 9)]
          (cond (= i 8) (get fish 0)
                (= i 6) (+ (get fish 0) (get fish 7))
                :else (get fish (inc i)))))
        (inc c))
      fish)))

(-> (slurp "./input.txt")
    (parse-input)
    (simulate-fish 80)
    ((fn [x] (reduce + x)))
    (println))
