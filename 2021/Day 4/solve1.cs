using System;
using System.Collections.Generic;
using System.Linq;

class Solve1 {
    static bool CheckBoard(List<List<bool>> board, int y, int x) {
        bool vert = true;
        bool hori = true;
        for(int i = 0; i < 5; i++){
            if(!board[y][i]){
                hori = false;
            }
            if(!board[i][x]){
                vert = false;
            }
        }
        return vert || hori;
    }

    static (int, int) CheckNumbers(
            List<List<List<int>>> boards,
            List<List<List<bool>>> selected,
            List<int> numbers
    ) {
        foreach(int num in numbers){
            for(int b = 0; b < boards.Count; b++){
                for(int y = 0; y < 5; y++){
                    for(int x = 0; x < 5; x++){
                        // Found matching number
                        if(boards[b][y][x] == num) {
                            selected[b][y][x] = true;
                            if(CheckBoard(selected[b], y, x)){
                                return (b, y, x, num);
                            }
                        }
                    }
                }
            }
        }
        return (0,0);
    }

    static void Main(string[] args) {
        // Reading file
        string[] lines = System.IO.File.ReadAllLines("./input.txt");
        // Parsing numbers
        List<int> numbers = lines[0].Split(",")
            .Select(x => int.Parse(x))
            .ToList();
        // Parsing boards
        /* var d = new Dictionary<string, List<(int b, int x, int y)>>(); */
        var boards = new List<List<List<int>>>();
        var selected = new List<List<List<bool>>>();
        for(int i = 2; i < lines.Length; i += 6) {
            var board = new List<List<int>>();
            // Row
            for(int y = 0; y < 5; y++) {
                var row = new List<int>();
                // Cells
                foreach(string x in lines[i+y].Split(" ")){
                    if(x == ""){
                        continue;
                    }
                    row.Add(int.Parse(x));
                }
                board.Add(row);
            }
            boards.Add(board);
            var selectedBoard = new List<List<bool>>{
                new List<bool> { false, false, false, false, false },
                new List<bool> { false, false, false, false, false },
                new List<bool> { false, false, false, false, false },
                new List<bool> { false, false, false, false, false },
                new List<bool> { false, false, false, false, false },
            };
            selected.Add(selectedBoard);
        }
        // Finding winner board
        (int board, int num) winner = CheckNumbers(boards, selected, numbers);
        // Calculate answer
        int sum = 0;
        for(int y = 0; y < 5; y++){
            for(int x = 0; x < 5; x++){
                if(!selected[winner.board][y][x]){
                    sum += boards[winner.board][y][x];
                }
            }
        }
        // Checking numbers
        Console.WriteLine(sum*winner.num);
    }
}
