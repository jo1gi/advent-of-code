(require '[clojure.string :as string])

(defn parse-input [input]
  (->> input
       (#(string/replace % #"\n" ""))
       (#(string/split % #","))
       (mapv #(string/split % #"="))
       (map #(get % 1))
       (map #(string/split % #"\.\."))
       (apply concat)
       (map #(Integer. %))
       ((fn [[xa xb ya yb]] {:xa xa :xb xb :ya ya :yb yb}))
       ))

(defn calc-highest-point [v]
  (reduce + (range 0 (+ v 1))))

(defn find-best-y [area]
  (->> (range 1 (Math/abs (area :ya)))
       (map calc-highest-point)
       (sort)
       (last)))

(-> (slurp "./input.txt")
    (parse-input)
    (find-best-y))
