(require '[clojure.string :as string])

(defn parse-input [input]
  (->> input
       (#(string/replace % #"\n" ""))
       (#(string/split % #","))
       (mapv #(string/split % #"="))
       (map #(get % 1))
       (map #(string/split % #"\.\."))
       (apply concat)
       (map #(Integer. %))
       ((fn [[xa xb ya yb]] {:xa xa :xb xb :ya ya :yb yb}))
       ))

(defn change-x-velocity [v]
  (+ v (cond (> v 0) -1
             (< v 0) 1
             :else 0)))

(defn is-valid-velocity? [vx vy area]
  (loop [vx vx
         vy vy
         x 0
         y 0]
    (cond (and (>= x (area :xa)) (<= x (area :xb)) (>= y (area :ya)) (<= y (area :yb))) true
          (or (> x (area :xb)) (< y (area :ya)) (and (= vx 0) (< x (area :xa)))) false
          :else (recur (change-x-velocity vx) (dec vy) (+ x vx) (+ y vy)))))

(defn find-initial-velocities [area]
  (->> (for [x (range 1 (+ 1(area :xb))) y (range (area :ya) (Math/abs (area :ya)))] [x y])
       (filter (fn [[x y]] (is-valid-velocity? x y area)))
       ))

(is-valid-velocity? 7 7 {:xa 20 :xb 30 :ya -10 :yb -5})

(->> (slurp "./input.txt")
     (parse-input)
     (find-initial-velocities)
     (count)
     )
