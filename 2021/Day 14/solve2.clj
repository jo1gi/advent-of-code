(require '[clojure.string :as string])

(defn parse-insertion-pair [m line]
  (let [x (string/split line #" -> ")
        a (get x 0)
        b (get a 0)
        c (get a 1)
        d (get x 1)]
       (conj m
             {a (->> [[b d] [d c]]
                      (map #(string/join "" %))
                      (vec))})))

(defn template-to-map [template]
  (->> template
       (partition 2 1)
       (map #(string/join "" %))
       (frequencies)))

(defn add-to-map [m parts c]
  (reduce
    (fn [m part]
      (if (contains? m part)
        (update m part + c)
        (update m part (constantly c))))
    m
    parts))

(defn run-steps [template pairs n]
  (loop [template (template-to-map template)
        i 0]
  (if (< i n)
    (recur
      (->> template
           (reduce (fn [a x] (add-to-map a (pairs (key x)) (val x)) ) {}))
      (inc i))
    template)))

(let [f (slurp "./input.txt")
      lines (string/split f #"\n")
      template (first lines)
      pairs (nthrest lines 2)]
  (->> pairs
    (reduce parse-insertion-pair {})
    (#(run-steps template % 40))
    (reduce (fn [a x] (add-to-map a (key x) (val x))) {})
    (#(update % (first template) + 1))
    (#(update % (last template) + 1))
    (map #(/ (val %) 2))
    (sort)
    (#(- (last %) (first %)))
    ))
