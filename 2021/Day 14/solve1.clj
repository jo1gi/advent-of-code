(require '[clojure.string :as string])

(defn parse-insertion-pair [m line]
  (let [x (string/split line #" -> ")]
       (conj m {(seq (get x 0)) (get x 1)})))

(defn apply-pair-insertion [part pairs]
  (conj [] (first part)
        (if (contains? pairs part)
          (pairs part)
          '()))
  )

(defn apply-pair-insertions [template pairs]
  (->> (partition 2 1 template)
       (map vec)
       (map #(apply-pair-insertion % pairs))
       (apply concat)
       (vec)
       (#(conj % (last template)))
       (string/join "")
       ))

(defn run-step [template pairs n]
  (loop [template template
         i 0]
    (if (< i n)
      (recur
        (apply-pair-insertions template pairs)
        (inc i))
      template)))

(let [f (slurp "./input.txt")
      lines (string/split f #"\n")
      template (first lines)
      pairs (nthrest lines 2)]
  (->> pairs
    (reduce parse-insertion-pair {})
    (#(run-step template % 10))
    (frequencies)
    (sort-by val)
    (map #(get % 1))
    (#(- (last %) (first %)))))

